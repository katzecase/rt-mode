;;; rt-mode.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Katzecase
;;
;; URL: https://gitlab.com/katzecase/rt-mode/
;;
;; Created: August 05, 2022
;; Modified: August 10, 2022
;; Version: 0.0.1
;; Package-Requires: ((emacs "24.3"))


;;; Commentary:

;; Tool for conver org-file to gantt-chart.

;;; code:
(defvar rt-max-indent-length 15)
(defvar rt-mark--event "E")
(defvar rt-mark--event-end "e")
(defvar rt-mark--event-start "s")

;; some tools that may be rebuilding wheel.
(defun rt-concat-str(str repeat)
  "Concat STR with REPEAT times."
  (cond
   ((<= repeat 0) "")
   ((= repeat 1) str)
   ((concat str (rt-concat-str str (- repeat 1))))))

(defun rt-get-time(target time-string)
  "Return TARGET from TIME-STRING as integer.
TARGET must be 'y for year, 'm for month, 'd for date.
TIME-STRING could be anything 'parse-time-string can reconized."
  (let ((parsed-time-str (parse-time-string time-string)))
    (cond
     ((equal target 'y) (nth 5 parsed-time-str))
     ((equal target 'm) (nth 4 parsed-time-str))
     ((equal target 'd) (nth 3 parsed-time-str)))))

(defun rt-time-less-p(A B)
  "Return if A is less than B.
A and B could be anything 'parse-time-string can reconized."
  (let* ((time-A (parse-time-string A))
         (time-B (parse-time-string B))
         (year-A (* 366 (nth 5 time-A)))
         (year-B (* 366 (nth 5 time-B)))
         (month-A (* 31 (nth 4 time-A)))
         (month-B (* 31 (nth 4 time-B)))
         (date-A (+ (nth 3 time-A) year-A month-A))
         (date-B (+ (nth 3 time-B) year-B month-B)))
    (< date-A date-B)))

;; main funcation
(defun rt-chart-year-and-month(array axis-begin)
  "Take a cons of ARRAY, return timeline chart begin with AXIS-BEGIN, as string."
  (let ((indent-amount 0)
        indent
        years
        time-begin
        output)
    (dolist (each array)
      (let* ((title (car each))
             (title-len (length title))
             (time (cdr each))
             (year (rt-get-time 'y (car time))))
        (when (>= year (rt-get-time 'y axis-begin))
          (progn
            (cond ((> title-len rt-max-indent-length) (setq indent-amount rt-max-indent-length))
                  ((> title-len indent-amount) (setq indent-amount title-len)))
            (push year years)
            (push (rt-get-time 'y axis-begin) years)
            (setq time-begin axis-begin)
            (push each output)))))
    (setq indent (rt-concat-str " " (+ indent-amount 1)))
    (when output (setq array (reverse output)))
    (setq years (sort (delete-dups years) #'<))
    (if (not years)
        ""
      (concat
       indent (rt-format-axis-year years) "\n"
       indent (rt-format-axis-month years) "\n"
       (mapconcat #'(lambda (x) (rt-format-timeline x indent-amount time-begin))
                  array
                  "\n")))))


;; axises
(defun rt-format-axis-year(years)
  "Take a list YEARS and return formatted axis-year as string."
  (let ((axis-full-month-gap-for-year "                                "))
    (mapconcat 'number-to-string years axis-full-month-gap-for-year)))

(defun rt-format-axis-month(years)
  "Take a list YEARS and teturn formatted axis-month as string."
  (let ((axis-full-month "01 02 03 04 05 06 07 08 09 10 11 12 "))
    (rt-concat-str axis-full-month (length years))))

;; timeline
(defun rt-format-timeline (title-and-date-cons title-length &optional time-begin)
  "Return timeline of TITLE-AND-DATE-CONS, align each timeline with TITLE-LENGTH.
If given, timeline will begin with TIME-BEGIN."
  (concat (rt-format-time-with-length (car title-and-date-cons) title-length)
          "|"
          (rt-format-event (cdr title-and-date-cons) time-begin)))

;; title
(defun rt-format-time-with-length (title title-length)
  "Return TITLE that gets trim or fill space to TITLE-LENGTH."
  (let ((form (format "%%->%da" title-length)))
    (format-spec form (list (cons ?a title)))))

;; event
(defun rt-format-event(date-cons &optional time-begin)
  "Return formatting event with DATE-CONS.
If given, begin with TIME-BEGIN, else begin with 'car DATE-CONS."
  (let* ((start (car date-cons))
         (end (cdr date-cons))
         mark-start
         mark-end
         (offset-start 0)
         (offset-end 0))
    (when start (let ((offset (rt-transform-time-to-offset start time-begin)))
                  (setq offset-start offset
                        mark-start (rt-transform-event start))))
    (when end (let ((offset (rt-transform-time-to-offset end start)))
                (setq offset-end offset
                      mark-end (rt-transform-event end))))
    (when (and start end) (if (= offset-start offset-end)
                              (setq mark-start rt-mark--event
                                    mark-end nil)
                            (setq mark-start (if (equal rt-mark--event
                                                        (rt-transform-event start))
                                                 rt-mark--event-start
                                               (rt-transform-event start))
                                  mark-end (if (equal rt-mark--event
                                                      (rt-transform-event end))
                                               rt-mark--event-end
                                             (rt-transform-event end))
                                  offset-end (- offset-end 1))))
    (concat (rt-concat-str " " offset-start) mark-start
            (rt-concat-str " " offset-end) mark-end)))

(defun rt-transform-event(time)
  "'string-match TIME and return event symbol."
    (cond ((string-match ">\\w" time) ">")
          ((string-match "\\b>" time) "}")
          ((string-match "<\\w" time) "<")
          ((string-match "\\b<" time) "{")
          (t rt-mark--event)))

(defun rt-transform-time-to-offset(time &optional time-begin)
  "Return offset amount of the TIME as integer.
Optainal TIME-BEGIN can be placed."
  (let* ((month-gap 3)
         (year-gap 12)
         (offset-time-begin (if (not time-begin) 0
                           (let* ((year-of-time (rt-get-time 'y time))
                                  (year-of-begin (rt-get-time 'y time-begin))
                                  (year-diff (- year-of-time year-of-begin))
                                  (offset-year (* month-gap year-diff year-gap))
                                  (offset-begin (rt-transform-time-to-offset time-begin)))
                             (- offset-year offset-begin))))
         (rt-month (rt-get-time 'm time))
         (offset-month (* month-gap (- rt-month 1)))
         (offset-date (let ((rt-date (rt-get-time 'd time)))
                     (cond ((<= rt-date 10) 0)
                           ((<= rt-date 20) 1)
                           ((<= rt-date 31) 2)))))
    (+ offset-time-begin
       offset-month
       offset-date)))

(provide 'rt-mode)
;;; rt-mode.el ends here
