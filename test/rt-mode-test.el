;;; rt-mode-test.el --- Description -*- lexical-binding: t; -*-
;;
;;; code:

(require 'ert)
(require 'rt-mode-test-helper)
(require 'rt-mode)

(ert-deftest test-concat-str()
  (should (equal
           (rt-concat-str "a " 1)
           "a "))
  (should (equal
           (rt-concat-str "b " 3)
           "b b b "))
  (should (equal
           (rt-concat-str " " 0)
           ""))
  (should (equal
           (rt-concat-str " " -1)
           "")))

(ert-deftest test-get-time()
    (should (eq
             (rt-get-time 'y "2030-01-01")
             2030))
    (should (eq
             (rt-get-time 'm "2030-02-03")
             2))
    (should (eq
             (rt-get-time 'd "2033-04-07")
             7)))

(ert-deftest test-time-less-p()
  (should (eql
           (rt-time-less-p "2030-01-01" "2030-01-02")
           t))
  (should (eql
           (rt-time-less-p "2030-01-01" "2030-02-01")
           t))
  (should (eql
           (rt-time-less-p "2030-01-01" "2031-01-01")
           t))
  (should (eql
           (rt-time-less-p "2030-07-09" "2030-01-01")
           nil))
  (should (eql
           (rt-time-less-p "2030-01-01" "2030-07-09")
           t)))

;;
;; axis
;;
(ert-deftest test-format-axis-year()
  (should (equal
           (rt-format-axis-year '(2030))
           "2030"))
  (should (equal
           (rt-format-axis-year '(2030 2031))
           "2030                                2031")))

(ert-deftest test-format-axis-month()
  (should (equal
           (rt-format-axis-month '(2030))
           "01 02 03 04 05 06 07 08 09 10 11 12 "))
  (should (equal
           (rt-format-axis-month '(2030 2031))
           "01 02 03 04 05 06 07 08 09 10 11 12 01 02 03 04 05 06 07 08 09 10 11 12 ")))

;;
;; timeline
;;
(ert-deftest test-format-timeline()
  (should (equal
           (rt-format-timeline (cons "cat" (cons "2030-01-01" nil)) 3)
           (format-rt "cat|%E")))
  (should (equal
           (rt-format-timeline (cons "dog" (cons "2030-02-04" nil)) 3)
           (format-rt "dog|   %E")))
  (should (equal
           (rt-format-timeline (cons "bird" (cons "2030-02-04" nil)) 4)
           (format-rt "bird|   %E")))
  (should (equal
           (rt-format-timeline (cons "cat" (cons "2030-02-04" nil)) 4)
           (format-rt "cat |   %E")))
  (should (equal
           (rt-format-timeline (cons "cat" (cons "2030-01-01" nil)) 4 "2029-03-01")
           (format-rt "cat |                              %E")))
  (should (equal
           (rt-format-timeline (cons "cat" (cons "2031-03-30" nil)) 3 "2030-01-01")
           (format-rt "cat|                                            %E")))
  (should (equal
           (rt-format-timeline (cons "beyond title length" (cons "2030-01-01" nil)) 4)
           (format-rt "beyo|%E"))))

;;
;; event
;;
(ert-deftest test-format-event()
  (should (equal
           (rt-format-event '("2030-01-01" . nil))
           (format-rt "%E")))
  (should (equal
           (rt-format-event '("2030-01-11" . nil))
           (format-rt " %E")))
  (should (equal
           (rt-format-event '(nil . "2030-01-01"))
           (format-rt "%E")))
  (should (equal
           (rt-format-event '(nil . "2030-01-21"))
           (format-rt "  %E")))
  (should (equal
           (rt-format-event '("2030-01-01" . "2030-01-11"))
           (format-rt "%s%e")))
  (should (equal
           (rt-format-event '("2030-01-01" . "2030-03-11"))
           (format-rt "%s      %e")))
  (should (equal
           (rt-format-event '("2030-01-01" . "2030-01-05"))
           (format-rt "%E"))))

(ert-deftest test-format-event-with-one-nil-with-condition()
  (should (equal
           (rt-format-event '(nil . nil))
           (format-rt "")))
  (should (equal
           (rt-format-event '(">2030-01-01" . nil))
           (format-rt "%g")))
  (should (equal
           (rt-format-event '("<2030-01-01" . nil))
           (format-rt "%l")))
  (should (equal
           (rt-format-event '(nil . "2030-01-01>"))
           (format-rt "%G")))
  (should (equal
           (rt-format-event '(nil . "2030-01-01<"))
           (format-rt "%L"))))

(ert-deftest test-format-event-with-one-condition()
  (should (equal
           (rt-format-event '("2030-01-01" . "2030-01-01<"))
           (format-rt "%E"))) ;;
  (should (equal
           (rt-format-event '("2030-01-01" . "2030-01-21<"))
           (format-rt "%s %L")))
  (should (equal
           (rt-format-event '("2030-01-01" . "2030-01-21>"))
           (format-rt "%s %G")))
  (should (equal
           (rt-format-event '(">2030-01-01" . "2030-01-21"))
           (format-rt "%g %e")))
  (should (equal
           (rt-format-event '("<2030-01-01" . "2030-01-21"))
           (format-rt "%l %e"))))

(ert-deftest test-format-event-with-two-condition()
  (should (equal
           (rt-format-event '(">2030-01-01" . "2030-01-01>"))
           (format-rt "%E"))) ;;
  (should (equal
           (rt-format-event '(">2030-01-01" . "2030-01-31>"))
           (format-rt "%g %G")))
  (should (equal
           (rt-format-event '(">2030-01-01" . "2030-01-31<"))
           (format-rt "%g %L")))
  (should (equal
           (rt-format-event '("<2030-01-01" . "2030-01-31>"))
           (format-rt "%l %G")))
  (should (equal
           (rt-format-event '("<2030-01-01" . "2030-01-31<"))
           (format-rt "%l %L"))))

(ert-deftest test-transform-event()
  (should (equal
           (rt-transform-event "2030-01-01")
           (format-rt "%E")))
  (should (equal
           (rt-transform-event ">2030-01-11")
           (format-rt "%g")))
  (should (equal
           (rt-transform-event "<2030-01-11")
           (format-rt "%l")))
  (should (equal
           (rt-transform-event "2030-01-11>")
           (format-rt "%G")))
  (should (equal
           (rt-transform-event "2030-01-11<")
           (format-rt "%L"))))


(ert-deftest test-transform-time-to-offset()
  (should (eq
           (rt-transform-time-to-offset "2020-01-01")
           0))
  (should (eq
           (rt-transform-time-to-offset "2020-01-11")
           1))
  (should (eq
           (rt-transform-time-to-offset "2020-01-10")
           0))
  (should (eq
           (rt-transform-time-to-offset "2020-02-04")
           3))
  (should (eq
           (rt-transform-time-to-offset "2021-02-04" "2020-01-01")
           (+ (* 12 3) 3)))
  (should (eq
           (rt-transform-time-to-offset "2020-02-04" "2020-02-01")
           0)))

(provide 'rt-mode-test)
;;; rt-mode-test.el ends here
