;;; chart-year-and-month.el --- Description -*- lexical-binding: t; -*-
;;
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'ert)
(require 'rt-mode-test-helper)
(require 'rt-mode)

(ert-deftest test-integration-chart-year-and-month-single-input()
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "2030-01-01" nil))) "2030-01-01")
           (format-rt
"    2030
    01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat|%E")))
  (should (equal
           (rt-chart-year-and-month (list (cons "dog" (cons "2030-01-01" nil))) "2030-01-01")
           (format-rt
"    2030
    01 02 03 04 05 06 07 08 09 10 11 12 \n\
dog|%E")))
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "2030-01-15" nil))) "2030-01-01")
           (format-rt
"    2030
    01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat| %E")))
  (should (equal
           (rt-chart-year-and-month (list (cons "bird" (cons "2030-02-15" nil))) "2030-01-01")
           (format-rt
"     2030
     01 02 03 04 05 06 07 08 09 10 11 12 \n\
bird|    %E")))
  (should (equal
           (rt-chart-year-and-month (list (cons "beyond max title length" (cons "2030-01-01" nil))) "2030-01-01")
           (format-rt
"                2030
                01 02 03 04 05 06 07 08 09 10 11 12 \n\
beyond max titl|%E"))))

(ert-deftest test-integration-chart-year-and-month-multiple-input()
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "2030-01-01" nil))
                                          (cons "dog" (cons "2030-01-15" nil)))
                                    "2030-01-01")
           (format-rt
"    2030
    01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat|%E
dog| %E")))
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "2030-01-05" nil))
                                          (cons "bird" (cons "2030-01-13" nil)))
                                    "2030-01-01")
           (format-rt
"     2030
     01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat |%E
bird| %E")))
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "2031-01-01" nil))
                                          (cons "bird" (cons "2030-01-15" nil)))
                                    "2030-01-01")
           (format-rt
"     2030                                2031
     01 02 03 04 05 06 07 08 09 10 11 12 01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat |                                    %E
bird| %E")))
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "2030-01-01" nil))
                                          (cons "bird" (cons "2031-01-05" nil)))
                                    "2030-01-01")
           (format-rt
"     2030                                2031
     01 02 03 04 05 06 07 08 09 10 11 12 01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat |%E
bird|                                    %E")))
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "2030-01-01" nil))
                                          (cons "bird" (cons "2030-01-15" nil))
                                          (cons "elephant" (cons "2031-03-30" nil))
                                          (cons "dog" (cons "2030-07-19" nil)))
                                    "2030-01-01")
           (format-rt
"         2030                                2031
         01 02 03 04 05 06 07 08 09 10 11 12 01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat     |%E
bird    | %E
elephant|                                            %E
dog     |                   %E"))))

(ert-deftest test-integration-chart-year-and-month-axis-begin-one-year-before()
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "2031-01-01" nil)))
                                    "2030-01-01")
           (format-rt
"    2030                                2031
    01 02 03 04 05 06 07 08 09 10 11 12 01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat|                                    %E"))))

(ert-deftest test-integration-chart-year-and-month-axis-begin-later-will-show-nothing()
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "2030-01-01" nil)))
                                    "2031-01-01")
           ""))
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "2031-01-01" nil))
                                          (cons "bird" (cons "2030-01-15" nil)))
                                    "2031-01-01")
           (format-rt
"    2031
    01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat|%E"))))

(ert-deftest test-integration-chart-year-and-month-timeline-order()
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "2031-01-01" nil))
                                          (cons "bird" (cons "2030-01-15" nil))
                                          (cons "elephant" (cons "2031-03-30" nil))
                                          (cons "dog" (cons "2031-07-19" nil)))
                                    "2030-01-01")
           (format-rt
"         2030                                2031
         01 02 03 04 05 06 07 08 09 10 11 12 01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat     |                                    %E
bird    | %E
elephant|                                            %E
dog     |                                                       %E"))))

(ert-deftest test-integration-chart-year-and-month-with-condition()
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons ">2030-01-01" nil)))
                                    "2030-01-01")
           (format-rt
"    2030
    01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat|%g")))
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "<2030-01-01" nil)))
                                    "2030-01-01")
           (format-rt
"    2030
    01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat|%l")))
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "2030-02-15>" nil)))
                                    "2030-01-01")
           (format-rt
"    2030
    01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat|    %G")))
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons "2030-01-01" "2030-02-03>")))
                                    "2030-01-01")
           (format-rt
"    2030
    01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat|%s  %G")))
  (should (equal
           (rt-chart-year-and-month (list (cons "cat" (cons ">2030-01-01" "2030-02-03>")))
                                    "2030-01-01")
           (format-rt
"    2030
    01 02 03 04 05 06 07 08 09 10 11 12 \n\
cat|%g  %G"))))



(provide 'chart-year-and-month)
;;; chart-year-and-month.el ends here
