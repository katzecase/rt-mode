;;; rt-mode-test-helper.el --- Description -*- lexical-binding: t; -*-
;;
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(defvar rt-marks
  (list (cons ?E "E")
        (cons ?e "e")
        (cons ?s "s")
        (cons ?g ">")
        (cons ?G "}")
        (cons ?L "{")
        (cons ?l "<")))

(defun format-rt (string-format)
  "An easier way to (format-spec STRING-FORMAT rt-marks)."
  (format-spec string-format rt-marks))


(provide 'rt-mode-test-helper)
;;; rt-mode-test-helper.el ends here
